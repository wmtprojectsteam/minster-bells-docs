# Minster Bells Docs

This repository contains the documentation for the Minster Bells and Music Player.  Note that this system was extensively upgraded and updated over the Winter Maintenance Period 2019/20 and earlier documents are provided at the minster-bells-and-webserver repository.

From 2022, the latest documentation has been consolidated into the Minster Bells Re-Engineering
Composite Document and this is the only document that will be maintained.
